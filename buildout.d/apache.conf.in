# need NameVirtualHost directive before including this.
# Apache server needs to set up global configs:
#
SSLCertificateFile /etc/pki/tls/certs/terranova.org.au.crt
SSLCertificateKeyFile /etc/pki/tls/private/terranova.org.au.key

#
# NameVirtualHost *:443
# NameVirtualHost *:80
#

# NB: Be sure to remove the default ssl vhost that CentOS's apache /etc/httpd/conf.d/ssl.conf creates. (JamesMills / 20130801)

ScriptAlias /seqcari/expresszip "${seqcari:web-directory}/seqcari.py"
Alias /seqcari ${seqcari:web-directory}
<Directory ${seqcari:web-directory}>
    Order allow,deny
    Allow from all
    Options +ExecCGI
    AddHandler cgi-script .cgi .py
</Directory>


<VirtualHost *:80>
    ServerName ${site:hostname}
    ServerAlias www.${site:hostname}
    ServerAdmin eresearch-services@griffith.edu.au
    UseCanonicalName Off

    # Redirect HTTP to HTTPS
    RewriteEngine On
    RewriteCond %{HTTPS} !=on
    RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]
</VirtualHost>

<VirtualHost *:443>
    ServerName ${site:hostname}
    ServerAlias www.${site:hostname}
    ServerAdmin eresearch-services@griffith.edu.au
    UseCanonicalName Off


    


    # Use separate log files for the SSL virtual host; note that LogLevel
    # is not inherited from httpd.conf.
    ErrorLog logs/ssl_error_log
    TransferLog logs/ssl_access_log
    LogLevel warn

    #   SSL Engine Switch:
    #   Enable/Disable SSL for this virtual host.
    SSLEngine on

    #   SSL Protocol Adjustments:
    #   The safe and default but still SSL/TLS standard compliant shutdown
    #   approach is that mod_ssl sends the close notify alert but doesn't wait for
    #   the close notify alert from client. When you need a different shutdown
    #   approach you can use one of the following variables:
    #   o ssl-unclean-shutdown:
    #     This forces an unclean shutdown when the connection is closed, i.e. no
    #     SSL close notify alert is send or allowed to received.  This violates
    #     the SSL/TLS standard but is needed for some brain-dead browsers. Use
    #     this when you receive I/O errors because of the standard approach where
    #     mod_ssl sends the close notify alert.
    #   o ssl-accurate-shutdown:
    #     This forces an accurate shutdown when the connection is closed, i.e. a
    #     SSL close notify alert is send and mod_ssl waits for the close notify
    #     alert of the client. This is 100% SSL/TLS standard compliant, but in
    #     practice often causes hanging connections with brain-dead browsers. Use
    #     this only for browsers where you know that their SSL implementation
    #     works correctly.
    #   Notice: Most problems of broken clients are also related to the HTTP
    #   keep-alive facility, so you usually additionally want to disable
    #   keep-alive for those clients, too. Use variable "nokeepalive" for this.
    #   Similarly, one has to force some clients to use HTTP/1.0 to workaround
    #   their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
    #   "force-response-1.0" for this.
    SetEnvIf User-Agent ".*MSIE.*" \
             nokeepalive ssl-unclean-shutdown \
             downgrade-1.0 force-response-1.0

    #   Per-Server Logging:
    #   The home of a custom SSL log file. Use this when you want a
    #   compact non-error SSL logfile on a virtual host basis.
    CustomLog logs/ssl_request_log \
              "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"

    # Fix HTTP_HOST being passed to Origin Server (Our Plone Instnace)
    # so that ``plone.jsonapi.core`` works as expected and properly
    # constructs ``api_url`` URI(s).
    ProxyPreserveHost On


    RequestHeader unset X_REMOTE_USER
    RequestHeader unset SHIB_PERSON_COMMONNAME
    RequestHeader unset SHIB_INETORGPERSON_MAIL

    RewriteEngine On

    

    RequestHeader set X_REMOTE_USER %{shared-token}e env=shared-token
    RequestHeader set SHIB_PERSON_COMMONNAME %{displayName}e env=displayName
    RequestHeader set SHIB_INETORGPERSON_MAIL %{mail}e env=mail

    # Redirect RIFCS Key for TerraNova
    RedirectMatch 301 ^/terranova https://terranova.org.au/
    # Rewrite to access oai
    RewriteRule ^/oai(.*)$ http://${hosts:moai}:${ports:moai}/oai$1 [L,P]
    # Rewrite to access supervisor web interface
    RewriteRule ^/_supervisor/(.*)$ http://${hosts:supervisor}:${ports:supervisor}/$1 [L,P]
    # Rewrite to access haproxy web interface
    RewriteRule ^/(_haproxy.*)$ http://${hosts:balancer}:${ports:balancer}/$1 [L,P]
    # Allow access to Zope root
    RewriteRule ^/_zope(.*)$ http://${hosts:cache}:${ports:cache}/VirtualHostBase/https/%{SERVER_NAME}:443/VirtualHostRoot/_vh__zope$1 [L,P]
    # Allow access to debug instance
    RewriteRule ^/_debug(.*)$ http://${hosts:instance-debug}:${ports:instance-debug}/VirtualHostBase/https/%{SERVER_NAME}:443/VirtualHostRoot/_vh__debug$1 [L,P]
    # Alloww access to Zeo Monitor
    RewriteRule ^/_zeo/(.*)$ http://${hosts:zeoserver}:${ports:zeomonitor}/$1 [L,P]
    # Allow access to RabbitMQ
    RewriteRule ^/_queue/(.*)$ http://${hosts:queue}:${ports:queue}/$1 [L,P]
    # Allow access to solr
    RewriteRule ^/solr/(.*)$ ajp://localhost:8009/solr/$1 [L,P]
    # Seqcari zip tool / replaced with CGI script
    #RewriteRule ^/seqcari/expresszip(.*)$ http://${seqcari:host}:${seqcari:port}/expresszip$1 [L,P]
    # Let Shibboleth, server status, seqcari, terranova  pass through normally
    RewriteCond %{REQUEST_URI} !^/(shibboleth-sp|Shibboleth.sso|server-(status|info)|seqcari|terranova)
    # and proxy everything else to our Virtulahost in Zope
    RewriteRule ^/(.*)$ http://${hosts:cache}:${ports:cache}/VirtualHostBase/https/%{SERVER_NAME}:443/${plone-sites:main}/VirtualHostRoot/$1 [L,P]

    <Location />
        #
        # Shibboleth Config
        #
        SSLRequireSSL

        AuthType shibboleth
        ShibRequestSetting requireSession Off
        Require shibboleth
    </Location>

    <Location /oai>
        # don't enforce shibboleth auth for oai
        ShibRequestSetting requireSession Off
        Require shibboleth
        ProxyPassReverse http://${hosts:moai}:${ports:moai}
    </Location>

    <Location /_supervisor/>
        # enforce shibboleth auth for supervisor
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
        ProxyPassReverse http://${hosts:supervisor}:${ports:supervisor}
    </Location>

    <Location /_haproxy>
        # /_haproxy?stats
        # /_haproxy?ping
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
        ProxyPassReverse http://${hosts:balancer}:${ports:balancer}
    </Location>

    <Location /_zope>
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
    </Location>

    <Location /_zope>
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
    </Location>

    <Location /_queue>
        # allow RabbitMQ access
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
        ProxyPassReverse http://${hosts:queue}:${ports:queue}
    </Location>

    <Location /_zeo>
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
        ProxyPassReverse http://${hosts:zeoserver}:${ports:zeomonitor}
    </Location>

    <LocationMatch /solr/select>
        Allow from all
        ProxyPassReverse ajp://localhost:8009/
    </LocationMatch>

    <LocationMatch /solr/(?!select)>
        ShibRequestSetting requireSession On
        ShibRequireAll On
        ShibAccessControl ${buildout:directory}/etc/accesscontrol.xml
        Require shibboleth
        ProxyPassReverse ajp://localhost:8009/
    </LocationMatch>

</VirtualHost>
