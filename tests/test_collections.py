from random import sample
from string import letters


from pytest import fixture, mark, xfail


from .test_auth import test_login  # noqa


@fixture(scope="module")
def collections(request):
    return []


@fixture(scope="function")
def collection(request, collections):
    name = "".join(sample(letters, 10))
    while name in collections:
        name = "".join(sample(letters, 10))

    collections.append(name)

    return name


@mark.incremental
def test_initial_repository(b, url):
    b.get(url + "/repository")
    assert b.current_url == url + "/repository"

    itemtitle = b.xpath("""//*[@id="itemtitle"]""")
    assert itemtitle.text == "Repository"


@mark.incremental
def test_add_collection(b, url, collection):
    try:
        test_initial_repository(b, url)
    except:
        xfail("test_initial_repository failed")

    addnew = b.xpath("""//*[@id="plone-contentmenu-factories"]/dt/a""")
    assert b.xpath("""//*[@id="plone-contentmenu-factories"]/dt/a/span[1]""").text.startswith(u"Add new")

    addnew.click()
    b.waitfor("xpath", """//*[@id="gu-repository-content-repositorycontainer"]""")
    repository_container = b.xpath("""//*[@id="gu-repository-content-repositorycontainer"]/span""")
    assert repository_container.text == "Repository Collection"

    repository_container.click()
    b.waitfor("xpath", """//*[@id="searchheader"]/h4""")
    assert b.xpath("""//*[@id="searchheader"]/h4""").text == "Upload to Repository"
    assert b.current_url == url + "/repository/++add++gu.repository.content.RepositoryContainer"

    title = b.xpath("//*/label[contains(text(), 'Title')]/../input[@type='text']")
    title.clear()
    title.send_keys(collection)

    b.xpath("""//input[@id='form-buttons-save']""").click()
    assert b.current_url == url + "/repository/{0:s}/view".format(collection.lower())
