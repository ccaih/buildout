# Module:   conftest
# Date:     16th December 2013
# Author:   James Mills, j dot mills at griffith dot edu dot au

"""pytest config for webui tests

Borrowed from: https://bitbucket.org/ccaih/ccav
"""


from os import environ


from pytest import fixture, xfail


from seleniumwrapper import create as create_selenium_driver


browsers = ["phantomjs"]


if "DISPLAY" in environ:
    browsers.extend([
        "firefox",
        "chrome",
        #(
        #    ("safari", "http://localhost:4444",),
        #    (
        #        ("acceptSslCerts", True),
        #    )
        #),
    ])


@fixture(scope="module", params=browsers)
def driver(request):
    if isinstance(request.param, tuple):
        args, kwargs = request.param
        kwargs = dict(kwargs)
    else:
        args = (request.param,)
        kwargs = {}

    b = create_selenium_driver(*args, **kwargs)

    request.addfinalizer(lambda *args: b.quit())

    return b


def pytest_addoption(parser):
    parser.addoption(
        "--url", action="store",
        default="http://localhost:8499/ccaih"
    )


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item


def pytest_runtest_setup(item):
    previousfailed = getattr(item.parent, "_previousfailed", None)
    if previousfailed is not None:
        xfail("previous test failed ({})".format(previousfailed.name))


@fixture(scope="session")
def url(request):
    return request.config.option.url


@fixture
def b(driver, url):
    b = driver
    b.set_window_size(1280, 1024)
    b.get(url)

    return b
