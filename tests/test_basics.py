def test_version_in_title(b):
    b.waitfor("xpath", "/html/head/title")
    assert b.title is not None
    assert b.title == "Plone site"
