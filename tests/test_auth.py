from pytest import mark


from urlparse import urlparse


@mark.incremental
def test_login(b, url):
    b.by_id("personaltools-login").click()

    ac_name = b.by_name("__ac_name")
    ac_password = b.by_name("__ac_password")

    ac_name.clear()
    ac_password.clear()

    ac_name.send_keys("admin")
    ac_password.send_keys("admin")

    b.button("Log in").click()

    assert b.by_id("username").text == "admin"


@mark.incremental
def test_logout(b, url):
    b.by_id("username").click()
    b.by_linktxt("Log out").click()

    assert urlparse(b.current_url).path == "/Shibboleth.sso/Logout"
