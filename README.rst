TerraNova Build Repository
==========================


This repository contains the TerraNova Plone Buildout configuration for development and deployment purposes.
This repository is the central place that build the actual TerraNova Plone-based Web Application which includes
configuring Plone with appropriate settings, workflows and content. This also includes the creation and configuration
of RDF-based Ontologies and a Fresnel Lens which drive most of the User Interface Form(s).


Getting Started with Development
--------------------------------

To get up and running quickly with development simply follow the following instructions:

.. note:: These instructions are for Mac OS X users only.

- Ensure you have `Brew <http://brew.sh/>`_ installed

  - If you already have Brew installed ensure it is up-to-date and running smoothly:

::
    
    $ brew doctor
    $ brew update
    
- Run the ``bootstrap.sh`` Bash script:

::
    
    $ ./bootstrap.sh
    

- Run ``fab build``:

::
    
    $ fab build
    
That it!
